<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<?php print $head; ?>
<title><?php print $head_title; ?></title>
<?php print $styles; ?>
<?php print $scripts; ?>
</head>
<body>
	<!-- Start Header -->
	<div id="header">
		<div class="container">
			<h1><a href="<?php print $front_page ?>" title="<?php print $site_name; ?>"><?php print $site_name; ?><span></span></a></h1>
			<hr />
			<?php if ($primary_links): ?>
			  <!-- top navigation -->
        <?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
      <?php endif; ?>
			<hr />
			<!-- banner message and building background -->
			<div id="banner">
			  <?php if ($site_slogan): ?>
				  <?php print $site_slogan; ?>
				<?php endif; ?>
			</div>
			<hr />
		</div>
	</div>
	<!-- Start Main Content -->
	<div id="main" class="container">
	  <?php if ($left): ?>
  		<!-- left column (products and features) -->
  		<div id="left">
        <?php print $left; ?>
  			<hr />
  		</div>
  	<?php endif; ?>
		<!-- main content area -->
		<div id="center">
		  <?php print $help; ?>
			<?php print $tabs; ?>
			<?php print $messages; ?>
			<?php if ($content_top): ?>
			  <?php print $content_top; ?>
			<?php endif; ?>
			<?php print $content; ?>
			<hr />
		</div>
		<?php if ($right): ?>
  		<!-- right sidebar -->
  		<div id="right">
        <?php print $right; ?>
  			<hr />
  		</div>
  	<?php endif; ?>
	</div>
	<?php if ($bottom || $bottomthree): ?>
  	<!-- Start Bottom Information -->
  	<div id="bottominfo">
		  <?php if ($bottom): ?>
		    <div class="container">
		      <?php print $bottom; ?>
		    </div>
		  <?php endif; ?>
		  <?php if ($bottomthree): ?>
		    <div class="three-container">
		      <?php print $bottomthree; ?>
		    </div>
		  <?php endif; ?>
  			<hr />
  	</div>
  <?php endif; ?>
	<!-- Start Footer -->
	<div id="footer">
		<div class="container">
		  <?php if ($footer_message): ?>
		    <?php print $footer_message; ?><br />
		  <?php endif; ?>
			<a id="designby" href="http://www.studio7designs.com/" title="Design by STUDIO7DESIGNS">WEB DESIGN BY STUDIO7DESIGNS</a>
			<?php if ($secondary_links): ?>
        <div id="footer-links">
          <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
        </div>
      <?php endif; ?>
		</div>
	</div>
<?php print $closure; ?>
</body>
</html>
