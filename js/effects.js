$(document).ready(function() {
  $('#left li.collapsed, #left li.leaf, #bottominfo li').mouseover(function() {
    $(this).css("background-color", "#E5E5E4");
  });
  $('#left li.collapsed, #left li.leaf').mouseout(function() {
    $(this).css("background-color", "#F1F0EF");
  });
  $('#bottominfo li').mouseout(function() {
    $(this).css("background-color", "#F1EEE7")
  });
});