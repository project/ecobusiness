Ecobusiness Theme for Drupal 6.x
Design by Studio7Designs http://www.studio7designs.com/
Theming by Theresaanna http://theresaanna.com

Customization and other information on the theme can be found at 
http://theresaanna.com/content/ecobusiness-theme

Many thanks to Studio7Designs for the great design and willingness to have their design themed for Drupal!